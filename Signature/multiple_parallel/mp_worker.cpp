#include "multiple_parallel/mp_worker.h"

#include <fstream>
#include <sstream>
#include <algorithm>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/filesystem.hpp>


namespace multiple_parallel
{

Worker::Worker(const boost::interprocess::file_mapping& mfile, size_t buffer_size, Func func, unsigned id) :
    mfile(mfile),
    buffer_size(buffer_size),
    func(func),
    id(id),
    taskPrepared(false),
    taskCompleted(true)
{
    try
    {
        terminate.store(false);

        std::ostringstream oss;
        oss << "~" << id << ".txt";
        outFileName = oss.str();
        output.open(outFileName.c_str(), std::ios_base::in | std::ios_base::out | std::ios_base::trunc);
        if (!output.is_open())
        {
            std::string e = "The system can't open the \"" + outFileName + "\" file for writing.";
            throw std::exception(e.c_str());
        }

        thread.reset(new std::thread(&Worker::execute, this));
    }
    catch (std::exception& e)
    {
        error = e.what();
    }
}

Worker::~Worker()
{
    if (thread)
    {
        {
            std::lock_guard<std::mutex> lk(mutex);
            taskPrepared = true;
            terminate.store(true);
            offset = 0;
            offsetStart = 0;
            offsetEnd = 0;
        }
        cv.notify_all();
        thread->join();
        output.close();
        boost::filesystem::remove(outFileName);
    }
}

bool Worker::isOk()
{
    return output.is_open() && thread.get();
}

std::string Worker::getError()
{
    return error;
}

void Worker::operator()(boost::interprocess::offset_t start, boost::interprocess::offset_t end)
{
    {
        std::lock_guard<std::mutex> lk(mutex);
        offset = 0;
        offsetStart = start;
        offsetEnd = end;
        taskPrepared = true;
        taskCompleted = false;
    }
    cv.notify_all();
}

void Worker::writeResult(std::ofstream& out)
{
    std::unique_lock<std::mutex> lk(mutex);
    cv.wait(lk, [this]{return this->taskCompleted; });
    char line[200];
    output.seekg(0);
    while (!output.eof())
    {
        output.getline(line, 200);
        if(line[0])
            out << line << '\n';
    }
}

void Worker::execute()
{
    using namespace boost;
    while (!terminate.load())
    {
        std::unique_lock<std::mutex> lk(mutex);
        cv.wait(lk, [this]{return this->taskPrepared; });
        taskPrepared = false;

        while (offsetStart < offsetEnd)
        {
            size_t block_size = static_cast<size_t>(std::min(static_cast<interprocess::offset_t>(buffer_size), offsetEnd - offsetStart));
            interprocess::mapped_region mregion(mfile, interprocess::read_only, offsetStart, block_size);
            output << func(mregion.get_address(), reinterpret_cast<unsigned char*>(mregion.get_address()) + block_size) << '\n';
            offsetStart += block_size;
        }

        taskCompleted = true;
        lk.unlock();
        cv.notify_all();
    }
}

}//namespace multiple_parallel
