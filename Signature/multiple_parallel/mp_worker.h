#ifndef _MULTIPLE_PARALLEL_WORKER_H_
#define _MULTIPLE_PARALLEL_WORKER_H_

#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <memory>
#include <string>
#include <fstream>
#include <boost/interprocess/file_mapping.hpp>

namespace multiple_parallel
{

class Worker
{
public:
    typedef std::string(*Func)(void* /*data_begin*/, void* /*data_end*/); 
public:
    Worker(const boost::interprocess::file_mapping& mfile, size_t buffer_size, Func func, unsigned id);
    ~Worker();
    Worker(const Worker&) = delete;
    Worker& operator=(const Worker&) = delete;

    bool isOk();
    std::string getError();
    void operator()(boost::interprocess::offset_t start, boost::interprocess::offset_t end);
    void writeResult(std::ofstream& out);
private:
    void execute();
private:
    const boost::interprocess::file_mapping& mfile;
    const size_t buffer_size;
    std::unique_ptr<unsigned char[]> buffer;
    std::unique_ptr<std::thread> thread;
    std::mutex mutex;
    std::condition_variable cv;
    bool taskPrepared;
    bool taskCompleted;
    std::atomic_bool terminate;
    Func func;
    const unsigned id;
    std::string error;
    boost::interprocess::offset_t offsetStart;
    boost::interprocess::offset_t offsetEnd;
    boost::interprocess::offset_t offset;
    std::string outFileName;
    std::fstream output;
};

}//namespace multiple_parallel

#endif // !_MULTIPLE_PARALLEL_WORKER_H_

