#ifndef _MULTIPLE_PARALLEL_H_
#define _MULTIPLE_PARALLEL_H_

#include "Params.h"

namespace multiple_parallel
{
void start(const Params& params);
}//namespace multiple_parallel

#endif // !_MULTIPLE_PARALLEL_H_
