#include "multiple_parallel.h"
#include "multiple_parallel/mp_worker.h"
#include "auxiliary/sums.h"

#include <iostream>
#include <boost/interprocess/file_mapping.hpp>
#include <boost/filesystem.hpp>

void multiple_parallel::start(const Params& params)
{
    using namespace boost;
    unsigned int desired_threads_count = std::thread::hardware_concurrency();
    if (desired_threads_count == 0)
    {
        desired_threads_count = 1;
        std::cout << "Can't get number of concurrent threads supported\n";
    }

    std::ofstream out;
    out.open(params.ofile);
    if (!out.is_open())
        throw std::invalid_argument("The system cannot open the file \"" + params.ofile + "\"");

    interprocess::file_mapping mfile(params.ifile.c_str(), interprocess::read_only);
    const interprocess::offset_t filesize = filesystem::file_size(params.ifile);

    const interprocess::offset_t amountBlocks = (filesize / params.block_size) + ((filesize % params.block_size) ? 1 : 0);
    unsigned threads_count = std::min(desired_threads_count, static_cast<unsigned>(amountBlocks));

    std::vector<std::shared_ptr<Worker> > workers;
    for (unsigned i = 0; i < threads_count; ++i)
    {
        try
        {
            std::unique_ptr<Worker> worker(new Worker(mfile, params.block_size, &aux::sums::sha1, i));
            if (worker && worker->isOk())
                workers.emplace_back(std::move(worker));
            else
                std::cerr << worker->getError() << "\n";
        }
        catch (std::bad_alloc& e)
        {
            std::cerr << e.what() << "\n";
        }
    }

    if (workers.empty())
        throw std::exception("The system cannot create threads.");
    else
        std::cout << "Number of used threads: " << workers.size() << '\n';

    const interprocess::offset_t blocksPerWorker = amountBlocks / workers.size();
    interprocess::offset_t unprocessedBlocks = amountBlocks % workers.size();

    interprocess::offset_t offset = 0;
    for (const auto& worker : workers)
    {
        interprocess::offset_t count = blocksPerWorker;
        if (unprocessedBlocks)
        {
            ++count;
            --unprocessedBlocks;
        }
        interprocess::offset_t size = std::min(count * params.block_size, filesize - offset);
        (*worker)(offset, offset + size);
        offset += size;
    }

    for (const auto& worker : workers)
    {
        worker->writeResult(out);
    }

    out.close();
}
