#ifndef _PARAMS_H_
#define _PARAMS_H_

#include <string>

struct Params
{
    Params();
    void parseArgs(int argc, const char* const argv[]);

    std::string ifile;
    std::string ofile;
    size_t block_size;
};

#endif // !_PARAMS_H_
