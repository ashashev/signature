#ifndef _UNITS_H_
#define _UNITS_H_

#include <string>

namespace aux
{
namespace units
{
    const size_t B = 1;
    const size_t kB = 1024 * B;
    const size_t MB = 1024 * kB;
    const size_t GB = 1024 * MB;

    size_t getMultiplucation(char unit);
    size_t parseSize(const std::string& size);
}//namespace units
}//namespace aux

#endif // !_UNITS_H_
