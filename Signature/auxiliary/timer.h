#ifndef _AUXILIARY_TIMER_H_
#define _AUXILIARY_TIMER_H_

namespace aux
{

template<typename Clock, typename F, typename... Args>
typename Clock::duration timer(F func, Args&&... args)
{
    auto start = Clock::now();
    func(args...);
    auto stop = Clock::now();
    return stop - start;
}

}//namespace aux

#endif //!_TIMER_H_
