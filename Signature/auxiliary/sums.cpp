#include "auxiliary/sums.h"

#include <sstream>
#include <iomanip>
#include <boost/uuid/sha1.hpp>

std::string aux::sums::sha1(void* data_begin, void* data_end)
{
    boost::uuids::detail::sha1 sha1;
    unsigned int digest[5];
    sha1.process_block(data_begin, data_end);
    sha1.get_digest(digest);
    std::ostringstream oss;
    oss << std::setfill('0') << std::hex;;
    for (const auto& i : digest)
        oss << std::setw(sizeof(unsigned int) * 2) << i;
    return oss.str();
}
