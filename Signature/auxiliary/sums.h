#ifndef _AUXILIARY_SUMS_H_
#define _AUXILIARY_SUMS_H_
#include <string>

namespace aux
{
namespace sums
{

std::string sha1(void* data_begin, void* data_end);

}//namespace sums
}//namespace aux

#endif ! _AUXILIARY_SUMS_H_