#include "Units.h"
#include <exception>
#include <regex>

size_t aux::units::getMultiplucation(char unit)
{
    switch (unit)
    {
    case 'k':
        return kB;
    case 'M':
        return MB;
    case 'G':
        return GB;
    }
    return 1;
}

size_t aux::units::parseSize(const std::string& size)
{
    std::regex reg("^\\s*([0-9]+)([kMG]{0,1})\\s*$");
    std::smatch res;
    if (!std::regex_match(size, res, reg))
    {
        std::string e = "The string '" + size + "' won't be parsed as contained size string."
                      + "size_string := decimal[size_suffix]\n"
                      + "size_suffix := B|k|M|G";
        throw std::invalid_argument(e);
    }

    return stol(res[1].str())*getMultiplucation(res[2].str()[0]);
}
