#include "Params.h"
#include "auxiliary/units.h"

#include <exception>

Params::Params():
    ifile(""),
    ofile(""),
    block_size(aux::units::MB)
{}

void Params::parseArgs(int argc, const char* const argv[])
{
    if (argc < 3)
    {
        std::string e = std::string("too few arguments.")
                      + "\nUsage: " + argv[0] + " input_file output_file [block_size]\n"
                      + "block_size := decimal[size_suffix]\n"
                      + "size_suffix := B|k|M|G";
        throw std::invalid_argument(e);
    }
    ifile = argv[1];
    ofile = argv[2];

    if (argc > 3)
        block_size = aux::units::parseSize(argv[3]);
}
