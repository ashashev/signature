#include "Params.h"
#include "auxiliary/timer.h"
#include "multiple_parallel/multiple_parallel.h"

#include <iostream>
#include <string>
#include <chrono>

int main(int argc, const char* const argv[])
{
    Params params;

    try
    {
        params.parseArgs(argc, argv);
        auto d = aux::timer<std::chrono::steady_clock>(multiple_parallel::start, params);
        std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(d).count() << "ms\n";
    }
    catch (std::exception& e)
    {
        std::cerr << "Error: " << e.what() << '\n';
    }
    return 0;
}
